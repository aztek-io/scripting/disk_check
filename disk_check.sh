#!/bin/bash
set -e
# Written by: Robert J.

#######################################
### Main Function #####################
#######################################

check_dir(){
    location="$1"

    while [[ -z "$location" ]]; do
        read -rp "Enter directory: " location;
    done

    print_system_information "$location"
    get_largest_directories "$location"
    get_largest_files "$location"
}


#######################################
### Generic Functions #################
#######################################

print(){
    var="$1";
    size="${#var}";

    printf -v sep "%${size}s" "-";
    printf '\n%s\n%s\n' "$var" "${sep// /-}";
}


#######################################
### Program Specific Functions ########
#######################################

print_system_information(){
    location="$1"

    print "$(hostname)";
    date;

    command -v lsblk > /dev/null && {
        print "Current Storage Configuration";
        lsblk;
    };

    print "Space Utilization";
    df -h "$location";

    print "Inode Utilization";
    df -i "$location";
}


get_largest_directories(){
    location="$1"

    print "Largest Directories";
    du -hcx -d 4 "$location" 2> /dev/null | \
        sort -hr | \
        head -n 15
}


get_largest_files(){
    location="$1"

    print "Largest Files";

    nice -n 19 \
        find "$location" -type f -print0 \
            2> /dev/null | \
                xargs -0 du -hk 2> /dev/null | \
                sort -rnk 1 | \
                head -n 20 | \
                awk '{
                    printf "%12.2f GB\t%s\n",
                    ($1/1024/1024),
                    $NF
                }' | \
                grep -Ev '0\.00 GB' | \
                cut -c -77;
}

#######################################
### Execution #########################
#######################################

check_dir "$@"
